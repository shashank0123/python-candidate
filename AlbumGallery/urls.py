from django.urls import path, include
from rest_framework import routers
from .views import AlbumViewSet, AlbumImageViewSet, UserViewSet

router = routers.DefaultRouter()
router.register('albums/', AlbumViewSet)
router.register('album_images', AlbumImageViewSet)
router.register('users', UserViewSet)

urlpatterns = [
    path('', include(router.urls))
]
