from django.apps import AppConfig


class AlbumgalleryConfig(AppConfig):
    name = 'AlbumGallery'
