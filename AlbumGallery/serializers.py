from rest_framework import serializers
from .models import Album, AlbumImage
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


class AlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Album
        fields = ('id', 'user', 'album_name', 'description',
                  'status', 'private', 'image')


class AlbumImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlbumImage
        fields = ('id', 'image_name', 'description',
                  'status', 'private', 'image')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user
