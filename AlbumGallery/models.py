from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.


class Album(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    album_name = models.CharField(max_length=100)
    description = models.TextField(max_length=1000)
    status = models.BooleanField(default=True)
    private = models.BooleanField(default=True)
    image = models.ImageField(upload_to='albums/')
    rating = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(10)]
    )

    def __str__(self):
        return album_name


class AlbumImage(models.Model):
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    image_name = models.CharField(max_length=100)
    description = models.TextField(max_length=1000)
    status = models.BooleanField(default=True)
    private = models.BooleanField(default=True)
    image = models.ImageField(upload_to='albums/')

    def __str__(self):
        return title
